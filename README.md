---------------
Retia Reader
The Sooper Spooky Halloween Edition
Peter Dreyer
------------

BeyondTrust Retina <http://www.beyondtrust.com/Products/RetinaCSThreatManagementConsole/> is network vulnerability analysis software designed to run on Windows, Mac OSX, and Linux systems.  The problem is that it lacks any sort of documentation on the XML-based file format it uses to export data.  This library is my effort to make parsing these files easier.  The API stated below is all that is available at the moment:

Functions:
 - parse() <-- This function, as the name implies, will parse one of Retina's XML files.  Give it the location of the file and go.

Classes:
 RetinaTree:
  This class contains all of the information from the given file in a more-or-less semantically useful format.
  Contains:
   job_name <-- The name of the job straight from the XML file.
   scanner_version <-- The version of the Retina scanner used to generate this file.  Currently direct from the file with no processing.
   start_time <-- The time the scan was started as a DateTime object
   end_time <-- The time the scan was ended calculated by adding the duration from the file to the start time.  Also returned as a DateTime object.
   hosts <-- Contains information on each host, each stored as a RetinaHost object.
  Functions:
   - down_hosts():
      Provides a list of all hosts which failed to be scanned, given in RetinaHost format.
 RetinaHost:
  This class contains all of the information on each host scanned.
  Contains:
   ipAddr <-- The ip address of the particular host
   state <-- The state the host was in as returned from Retina
   netBIOSName <-- The netBIOS name of the host.
   netBIOSDomain <-- The netbios domain of the host
   dnsName <-- The DNS name returned by the host.
   macAddr <-- The MAC address of the host, currently returned directly from Retina.
   OS <-- The operating system of the host, returned directly from Retina.
   cpe <-- The CPE of the host(?)
   audits <-- An array containing all of the audits performed on the host, each in RetinaAudit format.
 RetinaAudit:
  Contains:
   rthID <-- The rthID listed for this particular audit.
   cve <-- CVE found by the audit, if any.
   cce <-- CCE found by the audit, if any.
   name <-- Name of the audit
   description <-- Description of the audit.
