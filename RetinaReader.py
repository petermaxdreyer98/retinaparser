#------------------------------------
# Retina XML Parser
# The Sooper Spooky Halloween Edition
# Peter Dreyer
#------------------------------------

import os
import re
import xml.etree.ElementTree as ET
import datetime

class RetinaTree:
    num_hosts = 0

    # Metrics
    job_name = ""
    scanner_version = ""
    start_time = ""
    end_time = ""

    #Hosts
    hosts = []

    # Getters and Setters
    def down_hosts(this):
        dead_hosts = []
        # There has to be a better way to do this...
        skip_audits = False
        for host in this.hosts:
            for audit in host.audits:
                if audit.name == "System Not Responding on Forced Scan":
                    dead_hosts.append(host)
                    break

        return dead_hosts

class RetinaHost:
    def __init__(self):
        self.ipAddr = None
        self.state = None
        self.netBIOSName = None
        self.netBIOSDomain = None
        self.dnsName = None
        self.macAddr = None
        self.OS = None
        self.cpe = None
        
        self.audits = []

class RetinaAudit:
    def __init__(self):
        self.rthID = None
        self.cve = None
        self.cce = None
        self.name = None
        self.description = None

def parse(filename):

    tree = ET.parse(filename)
    root = tree.getroot()

    return_tree = RetinaTree()

    # Host and Metrics Branches
    metrics_branch = root.find("metrics")
    hosts_branch = root.find("hosts")

    # Job Name
    return_tree.job_name =  metrics_branch.find("jobName").text
    # Filename
    return_tree.filename = metrics_branch.find("fileName").text
    # Scanner Version
    return_tree.scanner_version = metrics_branch.find("scannerVersion").text
    # Attempted Host Count
    return_tree.attempted_hosts = metrics_branch.find("attempted").text

    # Start Time
    return_tree.start_time = _get_processed_start_time(metrics_branch)
    # End Time
    return_tree.end_time = _get_processed_end_time(metrics_branch)

    # Get Hosts
    return_tree.hosts = _get_hosts(hosts_branch)

    return return_tree

#Internal Functions
def _get_processed_start_time(metrics_branch):
    time = metrics_branch.find("start").text

    # String splitting
    numbers = re.split("/", time)

    numbers_continued = re.split(" ", numbers[2])

    # Date processing

    month = int(numbers[0])
    day = int(numbers[1])
    year = int(numbers_continued[0])

    # Time processing
    time_string =  re.split(":", numbers_continued[1])

    hour = int(time_string[0])
    minute = int(time_string[1])
    second = int(time_string[2])

    time_half = numbers_continued[2]

    if time_half == 'PM':
        hour = hour + 12

    processed_time = datetime.datetime(year, month, day, hour, minute, second)
    return processed_time

def _get_processed_end_time(metrics_branch):
    duration_string = metrics_branch.find("duration").text

    duration_array = re.split(" ", duration_string)

    total_days = int(duration_array[0][:-1])
    total_hours = int(duration_array[1][:-1])
    total_minutes = int(duration_array[2][:-1])
    total_seconds = int(duration_array[3][:-1])

    start_time = _get_processed_start_time(metrics_branch)

    end_time = start_time + datetime.timedelta\
               (days=total_days, hours=total_hours, minutes=total_minutes, seconds=total_seconds)
    return end_time

def _get_processed_attempted_hosts(xml_root):
    attempted_range = _get_attempted_hosts(xml_root)
    return attempted_range

def _get_hosts(host_branch):
    hosts = []
        
    for child in host_branch:
        current_host = RetinaHost() # Find a better way to do this
        
        # Top-level attributes
        ip = child.find("ip").text
        netBIOSName = child.find("netBIOSName").text
        netBIOSDomain = child.find("netBIOSDomain").text
        dnsName = child.find("dnsName").text
        macAddr = child.find("mac").text
        OS = child.find("os").text
        cpe = child.find("cpe").text
        
       # ...and assignments
        
        current_host.ipAddr = ip
        current_host.netBIOSName = netBIOSName
        current_host.netBIOSDomain = netBIOSDomain
        current_host.dnsName = dnsName
        current_host.macAddr = macAddr
        current_host.OS = OS
        current_host.cpe = cpe

        if netBIOSName != "N/A":
            current_host.netBIOSName = netBIOSName
        else:
            current_host.netBIOSName = None

        if netBIOSDomain != "N/A":
            current_host.netBIOSDomain = netBIOSDomain
        else:
            current_host.netBIOSDomain = None

        # Audits
        # First the audit nodes within the host
        audit_branches = child.findall("audit")

        for branch in audit_branches:
            audit = RetinaAudit()

            audit.rthID = branch.find("rthID").text
            audit.cve = branch.find("cve").text
            audit.cce = branch.find("cce").text
            audit.name = branch.find("name").text
            audit.description = branch.find("description").text
                
            current_host.audits.append(audit)
            
        hosts.append(current_host)
    
    return hosts
